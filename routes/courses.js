// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/courses');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] [POST] Route (Administrator Only)
	route.post('/', auth.verify, (req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			course: req.body,
		};
		if (isAdmin) {
			controller.addCourse(data).then(outcome => {
				res.send(outcome);
			});
		} else {
			res.send(`User Unauthorized to Proceed!`);
		}
	});
	
// [SECTION] [GET] Route
	// Retrieve All Courses (Administrator)
	route.get('/all', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		isAdmin ? controller.getAllCourses().then(outcome => res.send(outcome))
		: res.send(`Unauthorized User!`);
	});

	//  Retrieve All Active Courses
	route.get('/', (req, res) => {
		controller.getAllActive().then(outcome => {
			res.send(outcome);
		});
	});

	// Retrieve Single Course
	route.get('/:id', (req, res) => {
		let data = req.params.id;
		controller.getCourse(data).then(result => {
			res.send(result);
		});
	});
	
// [SECTION] [PUT] Route 
	// Update Course (Administrator)
	route.put('/:courseId', auth.verify,(req, res) => {
		let params = req.params;
		let body = req.body;
		if (auth.decode(req.headers.authorization).isAdmin) {
			controller.updateCourse(params, body).then(outcome => {
				res.send(outcome);
			});
		} else {
			res.send(`User Unauthorized`);
		};
	});

	// Course Archive (Administrator)
	route.put('/:courseId/archive', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
		isAdmin ? controller.archiveCourse(params).then(result => res.send(result))
		:res.send(`Unauthorized User`);
	});

// [SECTION] [DEL] Route (Administrator)
	route.delete('/:courseId', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params.courseId;
		isAdmin ? controller.deleteCourse(params).then(deleted => res.send(deleted))
		: res.send(`Unauthorized User`);
	});

// [SECTION] Export Route System
	module.exports = route;