// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');
	const auth = require('../auth');

// [SECTION] Functionalities [Create]
	// Register User
	module.exports.registerUser = (data) => {
		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let mobil = data.mobileNo;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, 10),
			mobileNo: mobil
		});

		return newUser.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return false;
			}
		});
	};

	// Email Checker
	module.exports.checkEmailExists = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {
			if (result.length > 0) {
				return 'Email already exists!';
			} else {
				return 'Email is still available!';
			};
		});
	};

	// Login (User Authentication)
	module.exports.loginUser = (reqBody) => {
		let uEmail = reqBody.email;
		let uPass = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				return false;
			} else {
			let passW = result.password;
				const isMatched = bcrypt.compareSync(uPass, passW);
				if (isMatched) {
					let dataNiUser = result.toObject();
					return {access: auth.createAccessToken(dataNiUser)}
				} else {
					return false;
				}
			}
		});
	};

	// Enroll User to a Course
	module.exports.enroll = async(data) => {
			let id = data.userId;
			let courseId = data.courseId;
			let isUserUpdated = await User.findById(id).then(user => {
				const courseFound = user.enrollments.find(userObj => userObj.courseId == courseId);
				if (!courseFound) {
					user.enrollments.push({courseId: courseId});
					return user.save().then((save, err) => {
						if (err) {
							return false;
						} else {
							return true;
						}
					});
				} else {
					return false;
				}
			});
			let isCourseUpdated = await Course.findById(courseId).then(course => {
				const userFound = course.enrollees.find(courseObj => courseObj.userId == id);
				if (!userFound) {
					course.enrollees.push({userId: id});
					return course.save().then((save, err) => {
						if (err) {
							return false;
						} else {
							return true;
						}
					});
				} else {
					return false;
				}
			});
		if (isUserUpdated && isCourseUpdated) {
			return true;
		} else {
			return `Enrollment Failed, go to registrar`;
		}
	};
	
// [SECTION] Functionalities [Retrieve]
	// Retrieve User profile
	module.exports.getProfile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};

// [SECTION] Functionalities [Update]
	// Set User as Admin
	module.exports.setAsAdmin = (userId) => {
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return `Successfully updated the user to Admin!`;
			} else {
				return `Updates failed to implement.....`;
			};
		});
	};

	// Set User as Non-Admin
	module.exports.setAsNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates).then((user, err) => {
			if (user) {
				return `Successfully updated the user to Non-Admin`;
			} else {
				return `Unable to update user information....`;
			};
		});
	};
	
// [SECTION] Functionalities [Delete]